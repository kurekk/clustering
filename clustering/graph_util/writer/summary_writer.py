import os
import tensorflow as tf
from datetime import datetime


class SummaryWriter(object):
    def __init__(self, graph_dir: str):
        self.graph_dir = graph_dir or os.getcwd()  # type: str
        self.__writer = None

    def create(self, graph: tf.Graph = None) -> tf.summary.FileWriter:
        os.makedirs(self.graph_dir, exist_ok=True)
        graph_path = os.path.join(self.graph_dir, "run-{}".format(datetime.now().time().strftime("%Y%m%d%H%M%S")))
        self.__writer = tf.summary.FileWriter(graph_path, graph or tf.get_default_graph())

        return self.__writer

    @property
    def writer(self) -> tf.summary.FileWriter:
        return self.__writer
