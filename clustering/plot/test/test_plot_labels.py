import numpy as np
import plotly.offline as py
import plotly.graph_objs as go

from unittest import TestCase


class TestPlotLabels(TestCase):
    def test_plot_labels(self):
        x = np.asarray([i for i in range(23)])
        y = np.asarray([1 for i in range(x.shape[0])])
        data = [go.Scatter(
            x=x,
            y=y,
            mode="markers",
            marker=dict(
                symbol="square",
                color="blue",
                size=10
            )
        )]

        layout = go.Layout(
            hovermode="closest"
        )
        fig = go.Figure(data=data, layout=layout)
        py.plot(fig, filename="labels.html")

    def test_plot_heat_map(self):
        x = np.asarray([i for i in range(23)])
        y = np.asarray([1 for i in range(x.shape[0])])
        z = np.asarray([1 for i in range(x.shape[0])])

        data = [go.Heatmap(x=x, y=y, z=z)]
        py.plot(data, filename="labels-heatmap.html")

    def test_plot_rect(self):
        layout = {
            "shapes": [{
                "type": "rect",
                "x0": 0,
                "y0": 0,
                "x1": 1,
                "y1": 1,
                "fillcolor": "blue"
            }]
        }
        py.plot({"data": [go.Scatter(
            x=[0.5],
            y=[0.5]
        )], "layout": layout}, filename="labels-rect.html")
