import numpy as np
import plotly.graph_objs as go
import plotly.offline as py

from clustering.plot.labels.color.labels_sequence_color_generator import LabelsSequenceColorGenerator


class DayHourLabelsPlot(object):
    def __init__(self, labels_color_generator: LabelsSequenceColorGenerator, hour_x_step=1):
        self.__labels_color_generator = labels_color_generator
        self.__hour_x_step = hour_x_step

    def plot(self, labels: np.ndarray, cluster_ids: np.ndarray, filename: str, auto_open=False):
        cluster_colors = self.__random_cluster_colors(cluster_ids)
        x = np.empty((len(labels),), dtype=np.float32)
        y = np.empty((len(labels),), dtype=np.float32)
        data_points = list()
        shapes = list()
        for i in range(len(labels)):
            next_label = labels[i]
            day = int(next_label[:2])
            hour = int(next_label[2:4])
            point = self.__create_point(day, hour)
            x[i] = point[0]
            y[i] = point[1]
            color = cluster_colors[cluster_ids[i]]
            box_shape = self.__create_box_shape(day, hour, color)
            data_points.append(self.__create_data_point(x[i], y[i], cluster_ids[i], color))
            shapes.append(box_shape)
        layout = self.__create_layout(shapes)
        py.plot(dict(data=data_points, layout=layout), filename=filename, auto_open=auto_open)

    def __create_box_shape(self, day: int, hour: int, color):
        return dict(
            type="rect",
            x0=hour,
            y0=day - 1,
            x1=hour + self.__hour_x_step,
            y1=day,
            fillcolor="rgb({},{},{})".format(color[0], color[1], color[2])
        )

    def __create_point(self, day: int, hour: int):
        return hour + self.__hour_x_step / 2, day - 0.5

    def __create_layout(self, shapes: list) -> dict:
        return dict(
            showlegend=False,
            shapes=shapes,
            xaxis=dict(
                title="Hour",
                autorange=False,
                range=[0, 24],
                autotick=False,
                ticks=[i for i in range(24)]
            ),
            yaxis=dict(
                title="Day",
                autorange=False,
                range=[0, 31],
                autotick=False,
                ticks=[i for i in range(1, 32, 1)]
            )
        )

    def __create_data_point(self, x, y, label, label_color):
        return go.Scatter(
            mode="markers",
            x=[x],
            y=[y],
            legendgroup=label,
            name=label,
            marker=dict(
                color="rgb({},{},{})".format(label_color[0], label_color[1], label_color[2])
            )
        )

    def __random_cluster_colors(self, clusters: np.ndarray) -> dict:
        unique_cluster_ids = np.unique(clusters)
        colors = self.__labels_color_generator.generate_colors(len(unique_cluster_ids))

        return dict([(cluster_id, colors[i]) for i, cluster_id in enumerate(unique_cluster_ids)])
