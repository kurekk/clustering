from abc import ABCMeta, abstractmethod


class LabelColorGenerator(metaclass=ABCMeta):
    @abstractmethod
    def generate_colors(self, color_cnt: int):
        raise NotImplementedError()
