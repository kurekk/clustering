from unittest import TestCase

from clustering.plot.labels.color.labels_random_color_generator import LabelRandomColorsGenerator


class TestLabelsRandomColorGenerator(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.__color_generator = LabelRandomColorsGenerator()

    def test_generate_colors(self):
        colors = self.__color_generator.generate_colors(4)
        self.assertEqual(len(colors), 4)
