from unittest import TestCase

from clustering.plot.labels.color.labels_sequence_color_generator import LabelsSequenceColorGenerator


class TestLabelsSequenceColorGenerator(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.labels_color_generator = LabelsSequenceColorGenerator()

    def test_labels_color_generator(self):
        colors_cnt = 4
        colors = self.labels_color_generator.generate_colors(colors_cnt)
        self.assertEqual(len(colors), colors_cnt)
