from unittest import TestCase


class Foo:
    def __init__(self, foo, id):
        self.foo = foo
        self.id = id


class TestMemAlloc(TestCase):
    def test_mem_alloc(self):
        foo = Foo(None, 0)
        for i in range(1, 5, 1):
            self.__print_mem_object_id(foo)
            foo = Foo(foo, i)
        self.__print_mem_object_id(foo)
        print(10 * "#")
        while foo is not None:
            self.__print_mem_object_id(foo)
            foo = foo.foo

    def __print_mem_object_id(self, o: Foo):
        print("Foo with %d id in memory %d, foo.foo in memory %d" % (o.id, id(o), id(o.foo)))
