import random

from typing import List

from clustering.plot.labels.color.labels_color_generator import LabelColorGenerator


class LabelsSequenceColorGenerator(LabelColorGenerator):
    def generate_colors(self, colors_cnt) -> List:
        colors = list()
        color_seed = random.randint(0, 255)
        color_value_space = 255 / colors_cnt
        for i in range(colors_cnt):
            new_color = color_seed + i * color_value_space
            if new_color > 255:
                new_color = new_color % 255
            colors.append([new_color] * 3)

        return colors
