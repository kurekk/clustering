import numpy as np

from clustering.plot.labels.color.labels_color_generator import LabelColorGenerator


class LabelRandomColorsGenerator(LabelColorGenerator):
    def generate_colors(self, color_cnt: int):
        return [np.random.choice(range(256), size=3).tolist() for i in range(color_cnt)]
