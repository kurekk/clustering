import os

import numpy as np
from psutil.tests import TestCase

from clustering.plot.labels.color.labels_random_color_generator import LabelRandomColorsGenerator
from clustering.plot.labels.labels_plot import DayHourLabelsPlot


class TestLabelsPlot(TestCase):
    @classmethod
    def setUpClass(cls):
        labels_color_generator = LabelRandomColorsGenerator()
        cls.__labels_plot = DayHourLabelsPlot(labels_color_generator)

    def test_plot_one_series(self):
        labels = np.asarray(
            [["{}{}".format(str.zfill(str(day), 2), str.zfill(str(hour), 2)) for hour in range(0, 24)] for day in
             range(1, 32, 1)]).flatten()
        clusters_list = list()
        for day in range(1, 32, 1):
            clusters_list.append(np.roll(np.asarray([[i] * 3 for i in range(0, 8)]).flatten(), day * 3))
        clusters = np.vstack(clusters_list).flatten()
        plot_path = self.__build_plot_path("plotoneseries.html")
        self.__labels_plot.plot(labels, clusters, plot_path, auto_open=True)
        self.assertTrue(os.path.isfile(plot_path))

    def __build_plot_path(self, filename: str):
        return os.path.join(os.path.dirname(__file__), filename)
