import tensorflow as tf
import numpy as np
import logging

from enum import Enum
from tensorflow.contrib import factorization
from tensorflow.contrib.factorization import KMeans

from clustering.graph_util.writer.summary_writer import SummaryWriter


class DistanceMetric(Enum):
    COSINE_DISTANCE = factorization.COSINE_DISTANCE
    SQUARED_EUCLIDEAN_DISTANCE = factorization.SQUARED_EUCLIDEAN_DISTANCE


class Kmeans(object):
    class ModelSpec(object):
        def __init__(self, x_placeholder: tf.placeholder, clusters_num: int, input_feat_nums: int,
                     distance_metric: DistanceMetric, use_mini_batch: bool):
            self.x_placeholder = x_placeholder  # type: tf.placeholder
            self.input_feat_nums = input_feat_nums
            self.clusters_num = clusters_num  # type: int
            self.distance_metric = distance_metric
            self.use_mini_batch = use_mini_batch

    class TrainingGraph(object):
        def __init__(self):
            self.all_scores = None
            self.cluster_idx = None
            self.scores = None
            self.cluster_centers_init = None
            self.init_op = None
            self.train_op = None
            self.avg_distance = None
            self.avg_cluster_var_placeholder = None

    class Output(object):
        def __init__(self):
            self.avg_distance = None
            self.cluster_idx = None
            self.all_scores = None

    class Summaries(object):
        def __init__(self):
            self.avg_distance = None  # type: tf.Tensor
            self.avg_cluster_var = None  # type: tf.Tensor

    def __init__(self, clusters_num: int, input_feat_nums: int, summary_witer: SummaryWriter,
                 distance_metric: DistanceMetric = None, use_mini_batch=False):
        self.__logger = logging.getLogger(__name__)
        self.__input = self.ModelSpec(None, clusters_num, input_feat_nums,
                                      distance_metric or DistanceMetric.COSINE_DISTANCE,
                                      use_mini_batch)
        self.__output = self.Output()
        self.__training_graph = self.TrainingGraph()
        self.__summaries = self.Summaries()
        self.__kmeans = None  # type: KMeans
        self.__summary_writer = summary_witer

    def train(self, x: np.ndarray, labels: np.ndarray, shuffle_input=True, steps=50):
        self.__create_model()
        if shuffle_input:
            self.__shuffle(x, labels)
        init_vars = tf.global_variables_initializer()
        with tf.Session() as session:
            self.__summary_writer.create()
            session.run(init_vars, feed_dict={self.__input.x_placeholder: x})
            session.run(self.__training_graph.init_op, feed_dict={self.__input.x_placeholder: x})
            for step in range(steps):
                self.__logger.debug("Training step {}".format(step))
                self.__train_step(x, step)

        return self.__assign_label_to_centroid(labels)

    def __train_step(self, x: np.ndarray, step: int):
        session = tf.get_default_session()
        summaries = tf.summary.merge([self.__summaries.avg_distance])
        g = self.__training_graph
        o = self.__output
        _, o.cluster_idx, out_summaries = session.run([g.train_op, g.cluster_idx, summaries],
                                                      feed_dict={self.__input.x_placeholder: x})
        self.__write_summaries(out_summaries, step)
        mean_cluster_var = self.__mean_cluster_variance(x, o.cluster_idx)
        summaries = tf.summary.merge([self.__summaries.avg_cluster_var])
        out_summaries = session.run(summaries,
                                    feed_dict={self.__training_graph.avg_cluster_var_placeholder: mean_cluster_var})
        self.__write_summaries(out_summaries, step)

    def __create_model(self):
        self.__input.x_placeholder = tf.placeholder(tf.float32, (None, self.__input.input_feat_nums), "input")
        self.__kmeans = KMeans(self.__input.x_placeholder, self.__input.clusters_num,
                               distance_metric=self.__input.distance_metric.value,
                               use_mini_batch=self.__input.use_mini_batch)
        g = self.__training_graph
        g.all_scores, g.cluster_idx, g.scores, g.cluster_centers_init, g.init_op, g.train_op = \
            self.__kmeans.training_graph()
        g.cluster_idx = g.cluster_idx[0]
        self.__training_graph.avg_distance = tf.reduce_mean(g.scores)
        self.__create_summaries()

    def __mean_cluster_variance(self, input_x: np.ndarray, predicted_clusters: np.ndarray):
        cluster_var = np.zeros((self.__input.clusters_num, self.__input.input_feat_nums))
        for cluster_num in np.unique(predicted_clusters):
            in_cluster = input_x[predicted_clusters == cluster_num]
            cluster_var[cluster_num] = np.var(in_cluster, axis=0)
        feature_var = np.mean(cluster_var, axis=0)
        mean_var = np.mean(feature_var)

        return mean_var

    def __create_summaries(self):
        self.__summaries.avg_distance = tf.summary.scalar("summary/kmeans/avg_distance",
                                                          self.__training_graph.avg_distance)

        self.__training_graph.avg_cluster_var_placeholder = tf.placeholder(tf.float64, name="cluster_var")
        self.__summaries.avg_cluster_var = tf.summary.scalar(
            "summary/kmeans/clusters/{}/mean_variance".format(self.__input.clusters_num),
            self.__training_graph.avg_cluster_var_placeholder)

    def __shuffle(self, x: np.ndarray, labels: np.ndarray):
        idx = [i for i in range(x.shape[0])]
        np.random.shuffle(idx)

        return x[idx], labels[idx]

    def __write_summaries(self, out_summaries, step_num: int):
        self.__summary_writer.writer.add_summary(out_summaries, step_num)
        self.__summary_writer.writer.flush()

    def __assign_label_to_centroid(self, input_labels: np.ndarray) -> dict:
        cluster_idx = self.__output.cluster_idx

        return dict([(l, np.argmax(cluster_idx[np.where(input_labels == l)])) for l in np.unique(input_labels)])
