import os
from typing import Tuple
from unittest import TestCase

import numpy as np
import tensorflow as tf

from clustering.graph_util.writer.summary_writer import SummaryWriter
from clustering.kmeans.kmeans import Kmeans


class TestKmeans(TestCase):
    FEATS_CNT = 24
    CLASS_NUM = 24 * 31
    SAMPLES_PER_CLASS = 100
    TFBOARD_DIR = os.path.join(os.path.dirname(__file__), "board")

    @classmethod
    def setUpClass(cls):
        cls.samples, cls.classes = cls.setUpData()

    def test_kmeans_clustering(self):
        summary_writer = SummaryWriter(self.TFBOARD_DIR)
        clusters_num = 30
        kmeans = Kmeans(clusters_num, self.FEATS_CNT, summary_writer)
        assigned_classes = kmeans.train(self.samples, self.classes, steps=50)
        self.assertTrue(len(assigned_classes), self.CLASS_NUM)
        assigned_clusters = list(assigned_classes.values())
        print("Assigned clusters: {}".format(set(assigned_clusters)))
        cluster_freq = [assigned_clusters.count(c) for c in set(assigned_clusters)]
        print(cluster_freq)

    def test_tf_where_statement(self):
        clusters = tf.constant(np.asarray([0, 1, 2, 0, 1, 1]))
        input_x = tf.constant(np.asarray([1, 7, 3, 5, 5, 11]))
        has_label = tf.equal(clusters, 1)
        with_label = tf.where(has_label)
        values_with_label = tf.gather(input_x, with_label)

        with tf.Session() as session:
            out_with_label = session.run(values_with_label)
        self.assertTrue(np.array_equal(out_with_label, np.asarray([[7], [5], [11]])))

    def test_calculate_var(self):
        x = np.asarray([
            [
                [1, 2, 3],
                [10, 10, 40],
                [5, 8, 8],
                [3, 1, 2]
            ]
        ], np.float32)
        x_placeholder = tf.placeholder(tf.float32)
        mean, var = tf.nn.moments(x_placeholder, axes=[0, 1])
        mean_var = tf.reduce_mean(var, axis=0)
        with tf.Session() as session:
            out_mean, out_var, out_mean_var = session.run([mean, var, mean_var], feed_dict={x_placeholder: x})
        print(out_mean, out_var, out_mean_var)

    @classmethod
    def setUpData(cls) -> Tuple[np.ndarray, np.ndarray]:
        all_feats, classes = list(), list()
        for c in range(cls.CLASS_NUM):
            all_feats.append(np.random.normal(100 + 20 * c, 20, cls.SAMPLES_PER_CLASS * cls.FEATS_CNT).reshape(
                (cls.SAMPLES_PER_CLASS, cls.FEATS_CNT)))
            classes.append(np.full(cls.SAMPLES_PER_CLASS, c))

        return np.concatenate(all_feats, axis=0), np.concatenate(classes, axis=0)
